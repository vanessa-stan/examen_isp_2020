import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class InterfataGrafica {

    private JFrame frame;
    private JTextField textField;
    private JTextField textField_1;
    private String allText = "";


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    InterfataGrafica window = new InterfataGrafica();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public InterfataGrafica() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1375, 748);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JButton btnNewButton = new JButton("submit");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {                   //in cadrul bloculul de try incercam sa luam numele fisierului din care facem citirea
                    //System.out.println(textField.getText()+".txt");
                    File myObj = new File(textField.getText()+".txt");
                    Scanner myReader = new Scanner(myObj);
                    while (myReader.hasNextLine()) {
                        String data = myReader.nextLine();
                        //System.out.println(data);
                        allText = allText + data;
                    }
                    myReader.close();

                    int count = 0;

                    for(int i = 0; i < allText.length(); i++) {         // numaram caracterele
                        if(allText.charAt(i) != ' ')
                            count++;
                    }
                    // facem dispaly la numarul total de carcatetre din fisierul citit
                    textField_1.setText("" + count);


                } catch (FileNotFoundException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }

            }
        });

        btnNewButton.setBounds(619, 500, 97, 25);
        frame.getContentPane().add(btnNewButton);

        textField = new JTextField();
        textField.setBounds(80, 268, 169, 39);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        textField_1 = new JTextField();
        textField_1.setBounds(581, 268, 169, 39);
        frame.getContentPane().add(textField_1);
        textField_1.setColumns(10);
    }
}